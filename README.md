### Gitlab-cicd
`目的：GitLab CIでテスト・ビルド・デプロイを自動化する機能を理解する`

`動作イメージ：ソースpush後にGitLab RunnerでWebアプリ（docker container）を自動デプロイする`

```
習得想定内容：
    Gitリポジトリの使用（dockerコンテナーdeploy、gitコマンド）
    golangでhttpサービス立ち上げ
    .gitlab-ci.ymlの編集
    gitlab-ciの使用（Piplines、Jobs）
    gitlab runnerのinstall、register、service deployment
```

こちらはネット公開の資料、概念的な部分について5分程度事前に読んでいただくとdemoに進めやすいと思う。

https://www.slideshare.net/ttyno/gitlab-cicd-197161419

一、事前準備

1.1. gitlab環境：Gitlabオフィシャル環境利用/GitLab(DSLX)利用、新規空っぽの`プロジェクト作成`

1.2. 開発PC：`docker`と`gitlab-runner`がインストール必要

1.3. テストコード：main.goサンプルコード

1.4. Dockerfile：ベースとするDockerイメージに対して実行する内容を記述しDockerイメージを生成する

1.5. .gitlab-ci.yml ：ファイルにパイプラインの構造と実行順序を定義する

`※1.3〜1.5は本プロジェクトからcloneし、中身を丸ごと自分が作った空っぽのプロジェクトにコピー`

二、環境設定

2.1. `トークン`確認

2.2. Runnerを`register`

2.3. 登録確認

三、ソースpush後にGitLab RunnerでWebアプリを自動ディプロイ、テストURL：http://127.0.0.1:8000

3.1. git pushで masterブランチを更新

3.2. 定義したパイプラインが自動で実行される

3.3 動作確認


### 一、事前準備

1. gitlab環境（テストのためofficialのGitlabリポジトリを利用したが、GitLab-DSLXも利用可能）
>Gitlabでテスト用の`プロジェクトを作成`
>>https://docs.gitlab.com/ce/gitlab-basics/create-project.html

2. `docker`と`gitlab-runner`をホスト機（開発PC）にインストール
> dockerインストール方法:
>> https://docs.docker.com/get-docker/

> gitlab-runnerインストール方法:
>> https://docs.gitlab.com/runner/install/
![img](./imgs/WX20200623-095019.png)
赤い部分を実行
![img](./imgs/WX20200623-101548.png)
※開発PCのOSに応じてクリックする。上記はインストールまで、runner登録は環境設定部分で実施する

3. `Main.go`テストコード、ここでの開発言語はGolangだが、他の言語も同じ流れになる
![img](./imgs/WX20200623-094232.png)

4. `Dockerfile`ファイル
```
# ベースイメージ
FROM golang:latest
# メンテナンス者
MAINTAINER Kyoku "g.qu@ntt.com"

# イメージ内の镜像中项目路径
WORKDIR $GOPATH/src/cicd.com/cicd-demo
# 本リポジトリのテストコードをイメージにコピー
COPY main.go $GOPATH/src/cicd.com/cicd-demo
# イメージ作成
RUN go build .

# コンテナが接続用にリッスンするポートを指定
EXPOSE 8000

# エントリポイント指定
ENTRYPOINT ["./cicd-demo"]
```

5. `.gitlab-ci.yml`ファイル
> tagsのところ必ずrunner登録時入力したtags名を変更してください。
```
stages:
  - deploy

docker-deploy:
  stage: deploy
  # ジョブ実行内容
  script:
    # Dockerfileからcicd-demoイメージ生成
    - docker build -t cicd-demo .
    # すでに動いているコンテナーがある場合削除
    - if [ $(docker ps -aq --filter "name=cicd-demo") ]; then docker rm -f cicd-demo;fi
    # イメージからコンテナーを起動、 ホストの8000番ポートをコンテナの8000番ポートへマッピング
    - docker run -d -p 8000:8000 --name cicd-demo cicd-demo
  tags:
    # Jobを実行するホスト指定（⭐️runner登録後最後必ず登録したtag名に修正）
    - local-runner
  only:
    # masterブランチが更新される場合のみ実行
    - master
```

### 二、環境設定

1. Token確認
> GitLabの「Settings」→「CI/CD」→「Runners」
図のtokenは後ほどrunner登録時使われる
![img](./imgs/WX20200623-102240.png)

2. Runner登録
> https://docs.gitlab.com/runner/register/index.html
![img](./imgs/WX20200623-095125.png)
※4番と5番は自分の好きな名前を入力、6番runner executorをshellに選んでください

> 登録後gitlab runnerを起動するように忘れないでください:https://docs.gitlab.com/runner/install/
![img](./imgs/WX20200623-134850.png)

3. 登録確認
> 下記の例では、`local-runner`というタグでrunnerを登録しました。
![img](./imgs/WX20200623-103405.png) 

### 三、ソースpush後にGitLab RunnerでWebアプリを自動ディプロイ
1. git pushで masterブランチを更新
```
git add *
git add .gitlab-ci.yml
git commit -m ✖✖✖✖
git status
git push︎
```
2. runner登録後、git pushでMasterブランチが更新されるたび、ジョブが自動で実行される。
> ジョブの実行結果は「CI/CD」の「Pipelines/Jobs」のところで確認する
![img](./imgs/WX20200623-103643.png)

3. 動作確認

最後にブラウザーから `http://127.0.0.1:8000` にアクセス確認
![img](./imgs/WX20200623-100922.png)

