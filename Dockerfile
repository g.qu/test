# ベースイメージ
FROM golang:latest
# メンテナンス者
MAINTAINER Kyoku "g.qu@ntt.com"

# イメージ内の镜像中项目路径
WORKDIR $GOPATH/src/cicd.com/cicd-demo
# 本リポジトリのテストコードをイメージにコピー
COPY main.go $GOPATH/src/cicd.com/cicd-demo
# イメージ作成
RUN go build .

# コンテナが接続用にリッスンするポートを指定
EXPOSE 8000

# エントリポイント指定
ENTRYPOINT ["./cicd-demo"]

